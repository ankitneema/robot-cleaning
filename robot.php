<?php
#! C:\wamp64\bin\php\

class FloorType{
	public  static $HARD_FLOOR 	= 'hard';
	public  static $CARPET_FLOOR 	= 'carpet';
}

class Clean
{
	private $floor;
    private $area 	= 0;
    private $dischargeTime 	= 60; //default discharge time
    private $chargeTime 	= 30; //default charge time
    private $sleepCount 	= 0; //default charge time

    public function __construct($floor = '', $area = 0)
    {
        $this->floor 	= $floor;
        $this->area 	= $area;
    }

    public function cleanApartment()
    { 
		$cleaningTime = $this->cleaningTime();
		for ($k = 1; $k < ($this->area); $k++) {
		    
		    echo "Claening in progress..\n";
		    sleep($cleaningTime);
		    
		    $this->sleepCount += $cleaningTime;
		  	if(($this->sleepCount%$this->dischargeTime) == 0)
		  	{
		  		echo "Wait robot charging in progress..\n";
		    	flush();
		    	sleep($this->chargeTime);
		  	}
		}
    }

    private function cleaningTime()
    {
    	if(strcmp($this->floor, FloorType::$HARD_FLOOR) ===0){
    		return 1;
    	}else if(strcmp($this->floor, FloorType::$CARPET_FLOOR) ===0){
    		return 2;
    	}else{
    		return 0;
    	}
    }
}

$getInput = getopt("", array("clean","floor:","area:"));
if(!empty($getInput))
{
	if(isset($getInput['floor']) && !in_array($getInput['floor'], [FloorType::$HARD_FLOOR,FloorType::$CARPET_FLOOR]))
	{
		print "Invalid Floor Type"; exit;
	}

	$area = (isset($getInput['area']) && !empty($getInput['area'])) ?$getInput['area'] :0;
	$cleaning = new clean($getInput['floor'],$area);
	print $cleaning->cleanApartment();
} else {
	print "Empty Input"; exit;
}